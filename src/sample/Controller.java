package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private DatePicker date;
    public RadioButton rbB;
    public GridPane mycontroller;
    public ComboBox<String> cblood;
    public ComboBox cbP;
    public RadioButton rbG;
    public RadioButton rbM;
    public ToggleGroup haha;
    public Button btnHead;
    public ImageView ivHead;

    public void doSeDay(ActionEvent actionEvent) {
        System.out.println(date.getValue());
    }

    public void doSex(ActionEvent actionEvent) {
        RadioButton radioButton = (RadioButton)actionEvent.getSource();
        System.out.println(radioButton.getText());
    }

    public void doSHead(ActionEvent actionEvent) {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));

        File selectedFile = fileChooser.showOpenDialog(mycontroller.getScene().getWindow());
        System.out.println(selectedFile.getAbsoluteFile());

        ivHead.setImage(new Image(selectedFile.toURI().toString()));

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> item= FXCollections.observableArrayList();
        item.addAll("O","A","B","AB");
        cblood.setItems(item);

        ObservableList<String> itemP= FXCollections.observableArrayList();
        String filename="C:\\Users\\CSIE-E518\\Desktop\\abc.txt";
        String line;

        try {
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while ((line=bufferedReader.readLine())!=null){
                    itemP.add(line);
            }
            bufferedReader.close();
            cbP.setItems(itemP);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
